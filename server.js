var cors = require('cors')
var fs = require('fs');

var a = '/semantic/dist/semantic.min.js'
var b = '/css/style_jeu.css'
var c = '/semantic/dist/semantic.min.css'


var app = require('express')();
app.use(cors());
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

server.listen(3000);

app.get('/', (req, res) => {
  res.sendFile(__dirname + "/vue/html/jeu.html")
})

app.get(a, (req,res) => {
  res.sendFile(__dirname + "/vue" + a)
})

app.get(b, (req,res) => {
  res.sendFile(__dirname + "/vue" + b)
})

app.get(c, (req,res) => {
  res.sendFile(__dirname + "/vue" + c)
})

users = [];
parties = [];


NBRMANCHES = 6

MOTS = ['chat', 'chien','pomme', 'ballon', 'giraffe', 'casquette', 'ordinateur', 'banane', 'france', 't-shirt', 'lunettes',
        'maison', 'coquillage', 'amour', 'philosophie', 'fort boyard', 'fourmi', 'paix', 'bilboquet', 'épée', 'titanic', 'dvd',
        'table', 'hulk', 'pokeball', 'racaillou', 'bouclier', 'chateau', 'bd', 'hamburger', 'guitare', 'mr propre', 'pizza',
        'pistolet', 'chapeau', 'ananas', 'chaussettes', 'tour effeil', 'bart', 'sabre laser', 'souris', 'multiplat', 'obelix',
        'bonbon', 'catamaran', 'papillon', 'salto', 'octogone', 'everest', 'poubelle', 'horloge', 'slack', 'usb', 'dent',
        'collier', 'camion', 'frite', 'fourchette', 'gomme', 'biere', 'bebe']

function generermots(liste, nbr) {
  i = 0
  mots = []
  l=liste.length
  while(i!=nbr){
    mot=liste[Math.floor(Math.random()*l)];
    if(!mots.includes(mot)){
      mots.push(mot)
      i++
    }
  }
  return mots
}

function pseudoExiste(users, name) {    //SAVOIR SI UN PSEUDO EST DEJA PRIS PAR UN JOUEUR
  for(user in users) {
    if(users[user].pseudo == name) {
      return true
    }
  }
  return false
}

function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}

// define interactions with client
io.sockets.on('connection', (socket) => {
  console.log('hey ! ' + socket.id)

  socket.on('mouseup', (X,Y,color,taille) => {
    socket.broadcast.emit('mouseup', X, Y, color, taille)
  })
  socket.on('mousedown', (X,Y,color,taille) => {
    socket.broadcast.emit('mousedown', X, Y, color, taille)
  })
  socket.on('mousemove', (X,Y,color,taille) => {
    socket.broadcast.emit('mousemove', X, Y, color, taille)
  })
  socket.on('mouseout', (X,Y,color,taille) => {
    socket.broadcast.emit('mouseout', X, Y, color, taille)
  })
  socket.on('erase', () => {
    socket.broadcast.emit('erase')
  })

  socket.on('login', (name) => {
    if(pseudoExiste(users,name)){
      socket.emit('loginres', false, '')
    } else {
      users.push({pseudo: name, id: socket.id})
      socket.emit('loginres', true, socket.id)
    }
  })

  socket.on('deco', () => {    // SI UN USER RETOURNE A LA PAGE DE CO
    i = 0
    for(user in users) {
      if(users[user].id == socket.id) {
        i = user
      }
    }
    users.splice(i,1)
  })

  socket.on('creer', (name) => {   //CREER UNE PARTIE
    mots = generermots(MOTS, NBRMANCHES)
    socket.join(name)
    parties.push({
      id: socket.id,
      admin: name,
      joueurs: [],
      infosPartie: {
        fermee: false,
        mots: mots,
        nbrj: 0,
        nbrok: 0
      }
    })
    socket.emit('creerres', mots)
  })

  socket.on('rejoindre', (name, pseudo) => {
    id = ''
    i = 0
    admin = ''
    for(p in parties) {
      if(parties[p].admin==name) {
        if(parties[p].infosPartie.fermee) {
          id = 'oups'
        } else {
          id = parties[p].id
          admin = parties[p].admin
          i = p
        }
      }
    }
    if(id == '') {
      socket.emit('rejoindreerr', 'Pas de partie à ce nom')
    } else if(id=='oups') {
      socket.emit('rejoindreerr', "La partie n'est plus accessible !")
    } else {
      parties[i].joueurs.push({pseudo: pseudo, id: socket.id, score: 0, success:false})
      parties[i].infosPartie.nbrj++
      if(parties[i].infosPartie.nbrj==6){
        parties[i].infosPartie.fermee = true
      }
      socket.join(admin)
      socket.to(admin).emit('rejoindrerestous', parties[i].joueurs)
      socket.emit('rejoindreressolo', admin, parties[i].joueurs, parties[i].infosPartie.mots)
    }
  })

  socket.on('partieAleatoire', (pseudo) => {
    id = ''
    i = -1
    admin = ''

    rand=[]
    l=parties.length
    for(i=0;i<l;i++){
      rand.push(i)
    }
    rand=shuffle(rand)
    res=false
    j=0
    while(!res && j<l){
      if(!parties[rand[j]].infosPartie.fermee){
        id = parties[j].id
        admin = parties[j].admin
        i = rand[j]
      }
      j++
    }
    if(i==-1){
      socket.emit('aleaerr', 'Pas de partie libre ...')
    } else {
      parties[i].joueurs.push({pseudo: pseudo, id: socket.id, score: 0, success:false})
      parties[i].infosPartie.nbrj++
      if(parties[i].infosPartie.nbrj==6){
        parties[i].infosPartie.fermee = true
      }
      socket.join(admin)
      socket.to(admin).emit('rejoindrerestous', parties[i].joueurs)
      socket.emit('aleasolo', admin, parties[i].joueurs, parties[i].infosPartie.mots)
    }
  })

  socket.on('pret', (name) => {
    for(p in parties) {
      if(parties[p].admin==name) {
        parties[p].infosPartie.fermee = true
      }
      for(j in parties[p].joueurs){
        parties[p].joueurs[j].success = false
      }
    }
    socket.to(name).emit('pret')
    socket.emit('pret')
  })

  socket.on('trouver', (name, room, tps, manche) => {
    i = 0
    for(partie in parties) {
      if(parties[partie].admin == room) {
        i = partie
      }
    }
    if(i>-1){
      for(j in parties[i].joueurs) {
        if(parties[i].joueurs[j].pseudo == name){
          parties[i].joueurs[j].success = true
          parties[i].joueurs[j].score += tps
          parties[i].infosPartie.nbrok++
          if(parties[i].infosPartie.nbrok==parties[i].infosPartie.nbrj){
            if(manche != NBRMANCHES-1){
              socket.to(room).emit('tpsfinish')
              socket.emit('tpsfinish')
            } else {
              socket.to(room).emit('partiefinish')
              socket.emit('partiefinish')
            }
            socket.to(parties[i].id).emit('stop')
            parties[i].infosPartie.nbrok = 0
          }
        }
      }
    }
    socket.to(room).emit('trouverres', parties[i].joueurs)
    socket.emit('trouverres', parties[i].joueurs)
  })

  socket.on('tpsmoins', (room) => {
    socket.to(room).emit('tpsmoins')
    // socket.emit('tpsmoins')
  })

  socket.on('tpsfinish', (room, manche) => {
    i = 0
    for(partie in parties) {
      if(parties[partie].admin == room) {
        i = partie
      }
    }
    if(manche != NBRMANCHES-1){
      socket.to(room).emit('tpsfinish')
      socket.emit('tpsfinish')
    } else {
      socket.to(room).emit('partiefinish')
      socket.emit('partiefinish')
    }
    // socket.to(parties[i].id).emit('stop')
    parties[i].infosPartie.nbrok = 0
  })

  socket.on('killpartie', (room) => {
    console.log(parties)
    i=-1
    for(partie in parties){
      if(parties[partie].admin == room){
        i=partie
      }
    }
    if(i>-1){
      parties.splice(i,1)
    }
    socket.leave(room)
    console.log(parties)
  })

  socket.on('leaveroom', (room) => {
    socket.leave(room)
  })

  socket.on('disconnect', () => {
    i = 0
    k = 0
    name=""
    for(user in users) {
      if(users[user].id == socket.id) {
        i = user
        name=users[user].pseudo
      }
    }
    for(p in parties) {
      if(parties[p].admin == name) {
        k = p
        socket.to(name).emit('cbarre')
      }
      for(j in parties[p].joueurs){
        if(parties[p].joueurs[j].id==socket.id){
          socket.to(parties[p].admin).emit('joueurscbarre', j)
        }
      }
    }
    if(i>-1){
      users.splice(i,1)
    }
    if(k>-1){
      parties.splice(k,1)
    }
  })
});
